package kafka_basics;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

import com.google.gson.JsonParser;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer {
    static Logger logger = LoggerFactory.getLogger(Consumer.class);

    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(Consumer.class);
        // create consumer configs
        Properties props = new Properties();

        final String bootstrapServer = "127.0.0.1:9092";
        // https://kafka.apache.org/documentation/#consumerconfigs
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props); // we gonna raed strings key
                                                                                           // and value

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("stopping application...");
            consumer.close();
            logger.info("consumer closed...");
        }));
        // subscribe
        consumer.subscribe(Arrays.asList("twitter"));
        logger.info("into while");
        while (true) {
            logger.info("consuming...");
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
            
            for (ConsumerRecord<String, String> record : records) {
                // idempotence
                String kafkaId = record.topic() + "_" + record.partition() + "_" + record.offset();
                String id = getTweetId(record.value());
                logger.info("tweet id: " + id);
                logger.info("kafka id: " + kafkaId);
                // logger.info("-------------------------");
                // logger.info("key: " + record.key() + ", value: " + record.value() + ", offset: " + record.offset()
                //         + ", partition: " + record.partition());
                // logger.info("-------------------------");
            }
            // if(records.isEmpty()){
            // System.out.println(records);
            // logger.info("close because is empty");
            // consumer.close();
            // }
        }
    }

    private static String getTweetId(String json) {
        String id = JsonParser.parseString(json).getAsJsonObject().get("id_str").getAsString();
        return id;
    }

}
