package kafka_basics;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Producer {
    public static void main(String[] args) {
        
        Logger logger = LoggerFactory.getLogger(App.class);

        final String bootstrapServer = "127.0.0.1:9092";
        // set kafka properties
        // https://kafka.apache.org/documentation/#producerconfigs
        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("stopping application...");
            producer.close();
            logger.info("Producer closed...");
        }));

        for (int i = 0; i < 10; i++) {

            // create message
            final String topic = "first_topic";
            final String value = "Hello world from java " + i;
            final String key = "id_" + i;
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, key, value);
            // send message
            logger.info("key: " + key);
            try {
                producer.send(record, new Callback() {

                    @Override
                    public void onCompletion(RecordMetadata metadata, Exception exception) {
                        if (exception != null)
                            logger.error("Some error happened when trying to send record: ", exception);
                        else {
                            logger.info("\n\n------Received new data------\n" + "topic: " + metadata.topic() + "\n"
                                    + "offset: " + metadata.offset() + "\n" + "partition: " + metadata.partition()
                                    + "\n" + "timestamp of the record: " + metadata.timestamp() + "\n"
                                    + "------------------------------------");
                        }
                    }

                }).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } // ->async | dont put get in production!!
        }
        // "wait" for producer to send data
        producer.flush();
        // close producer (avoid memory leak)
        producer.close();
        logger.info("-- END --");
    }
}
