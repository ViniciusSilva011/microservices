package kafka_elastic_search;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

import com.google.gson.JsonParser;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ElasticRequestType {
    final static String BULK = "bulk";
    final static String ONE_AT_A_TIME = "one at a time";
}

/**
 * Hello world!
 */
public final class App {
    static Logger logger = LoggerFactory.getLogger(App.class);
    static String elasticRequestType = ElasticRequestType.BULK;

    private App() {
    }

    private static RestHighLevelClient makeElasticSearchClient() {
        String hostname = "kafka-9491111255.eu-west-1.bonsaisearch.net";
        String username = "tm25vj7o2e";
        String password = "bvr010y0le";

        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));

        RestClientBuilder builder = RestClient.builder(new HttpHost(hostname, 443, "https"))
                .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                    @Override
                    public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                        return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                    }
                });
        RestHighLevelClient client = new RestHighLevelClient(builder);
        return client;
    }

    public static void addShutdownHook(RestHighLevelClient client) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("stopping application...");
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.info("client closed...");
        }));
    }

    public static String getKafkaId(ConsumerRecord<String, String> record) {
        return record.topic() + "_" + record.partition() + "_" + record.offset();
    }

    public static String getIdempotentId(ConsumerRecord<String, String> record) {
        String idempotentOption = "second";
        String kafkaId = getKafkaId(record);
        String tweetId = getTweetId(record.value());

        return idempotentOption == "first" ? tweetId : idempotentOption == "second" ? kafkaId : "";
    }

    public static IndexRequest getIndexRequest(ConsumerRecord<String, String> record, String idempotentId) {
        return new IndexRequest("twitter").id(idempotentId).source(record.value(), XContentType.JSON);
    }

    public static void doIndexRequestOneAtATime(IndexRequest indexRequest, RestHighLevelClient client) {
        IndexResponse resp;
        try {
            // run request
            resp = client.index(indexRequest, RequestOptions.DEFAULT);

            final String id = resp.getId();
            logger.info("id: " + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void waitSomeSeconds() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void doBulkRequest(RestHighLevelClient client, BulkRequest bulkRequest) {
        try {
            BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) {
        RestHighLevelClient client = makeElasticSearchClient();

        addShutdownHook(client);

        KafkaConsumer<String, String> consumer = getKafkaConsumer();
        consumer.subscribe(Arrays.asList("twitter"));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(3000));

            Integer recordsCount = records.count();

            logger.info("Received " + recordsCount + " records");

            BulkRequest bulkRequest = new BulkRequest();

            for (ConsumerRecord<String, String> record : records) {
                logger.info(getKafkaId(record));
                try {
                    String idempotentId = getIdempotentId(record);
                    IndexRequest indexRequest = getIndexRequest(record, idempotentId);

                    if (elasticRequestType == ElasticRequestType.ONE_AT_A_TIME) {
                        doIndexRequestOneAtATime(indexRequest, client);
                    } else if (elasticRequestType == ElasticRequestType.BULK) {
                        bulkRequest.add(indexRequest);
                    }
                    waitSomeSeconds();
                } catch (NullPointerException e) {
                    // TODO: handle exception
                    logger.warn("NullPointerException | record.value() | Skipping data: " + record.value());
                }

            }
            if (records.count() > 0) {
                if (elasticRequestType == ElasticRequestType.BULK) {
                    doBulkRequest(client, bulkRequest);
                }

                logger.info("Committing offsets...");
                consumer.commitSync();
                logger.info("Offsets committed.");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // if (records.isEmpty()) {
            // logger.info("closing because is empty");
            // consumer.close();
            // logger.info("Closed");
            // return;
            // }
        }

    }

    private static String getTweetId(String json) {
        // .parseString(json)
        String id = JsonParser.parseString(json).getAsJsonObject().get("id_str").getAsString();
        return id;
    }

    static private KafkaConsumer<String, String> getKafkaConsumer() {

        Logger logger = LoggerFactory.getLogger(App.class);
        // create consumer configs
        Properties props = new Properties();

        final String bootstrapServer = "127.0.0.1:9092";
        // https://kafka.apache.org/documentation/#consumerconfigs
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "dale-10");
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "5");

        // create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props); // we gonna read strings key
                                                                                           // and value
        // consumer.assign(); // assign to a specific partition
        // consumer.round
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("stopping application...");
            consumer.close();
            logger.info("consumer closed...");
        }));

        return consumer;
    }
}
