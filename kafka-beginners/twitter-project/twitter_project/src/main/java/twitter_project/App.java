package twitter_project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.print.DocFlavor.STRING;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    public App() {
    }

    Logger logger = LoggerFactory.getLogger(App.class.getName());
    static ArrayList<String> searchTerms = Lists.newArrayList("man");

    public static void main(String[] args) {
        new App().run();
    }

    private void run() {
        Kafka kafka = new Kafka();
        /**
         * Set up your blocking queues: Be sure to size these properly based on expected
         * TPS of your stream
         */
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100);
        Client client = App.createTwitterClient(msgQueue);
        client.connect();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("stopping application...");
            kafka.endProducer();
            client.stop();
            logger.info("Client stopped...");

        }));

        while (!client.isDone()) {
            String msg = null;
            try {
                msg = msgQueue.poll(5, TimeUnit.SECONDS);

            } catch (InterruptedException e) {
                e.printStackTrace();
                client.stop();
            }
            if (msg != null) {
                kafka.sendRecord(msg);
                this.logger.info(msg);
            }
        }

        kafka.endProducer();
        this.logger.info("End of application");
    }

    private static Client createTwitterClient(BlockingQueue<String> msgQueue) {

        // BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1000);

        /**
         * Declare the host you want to connect to, the endpoint, and authentication
         * (basic auth or oauth)
         */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        // Optional: set up some followings and track terms
        List<String> terms = searchTerms;
        hosebirdEndpoint.trackTerms(terms);

        final String consumerKey = "zqZaEIibNeyB716KIYmymQynp";
        final String consumerSecret = "P6OkOUk8tpMHl7fYEHYJWh3B7CaFKJ2ZWvbROKs1M9v7mI9mTe";
        final String token = "895721412542976000-AYMZKrhKjxHX6kgmSGdXcLe9tcROqk6";
        final String tokenSecret = "WUbJIFFZtvWFcxfTPckYUnkcGeUXpdD7o7PSob87zmLUJ";

        // These secrets should be read from a config file
        Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, token, tokenSecret);

        ClientBuilder builder = new ClientBuilder().name("vinicius") // optional: mainly for the logs
                .hosts(hosebirdHosts).authentication(hosebirdAuth).endpoint(hosebirdEndpoint)
                .processor(new StringDelimitedProcessor(msgQueue)); // optional: use this
                                                                    // events

        Client hosebirdClient = builder.build();
        // Attempts to establish a connection.

        return hosebirdClient;
    }
}
