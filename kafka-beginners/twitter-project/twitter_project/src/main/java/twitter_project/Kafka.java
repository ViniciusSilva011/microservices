package twitter_project;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Kafka {
    KafkaProducer<String, String> kafkaProducer;
    Logger logger = LoggerFactory.getLogger(Kafka.class.getName());
    
    public Kafka() {
        this.makeProducer();
    }

    public Kafka makeProducer() {
        Properties config = new Properties();

        try {
            config.put("client.id", InetAddress.getLocalHost().getHostName());
            config.put("bootstrap.servers", "127.0.0.1:9092");
            config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            // safe producer
            config.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
            config.put(ProducerConfig.ACKS_CONFIG, "all");
            config.put(ProducerConfig.RETRIES_CONFIG, Integer.toString(Integer.MAX_VALUE));
            config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");
            // high throughput producer (at the expense of a bit of latency and CPU usage)
            config.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
            config.put(ProducerConfig.LINGER_MS_CONFIG, "20");
            config.put(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(32*1024)); // 32 KiB batch size



        } catch (UnknownHostException e1) {
            e1.printStackTrace();
        }

        this.kafkaProducer = new KafkaProducer<String, String>(config);

        return this;
    }

    public void sendRecord(String msg) {
        final ProducerRecord<String, String> record = new ProducerRecord<String, String>("twitter", 3, "tweet", msg );

        this.kafkaProducer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                if (exception != null) {
                    logger.error("onCompletion error", exception);
                }
            }
        });
    }

    public void endProducer() {
        this.kafkaProducer.flush();
        logger.info("Producer flushed...");
        this.kafkaProducer.close();
        logger.info("Producer closed...");
    }
}
