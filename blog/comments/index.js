const express = require('express');
const axios = require('axios');
const { randomBytes } = require('crypto');
const app = express();
const cors = require('cors');

app.use(express.json());
app.use(cors());

const commentsByPostId = {};

app.get('/posts/:id/comments', (req, res) => {
    res.status(200).send(commentsByPostId[req.params.id] || []);

});

app.post('/posts/:id/comments', async (req, res) => {
    const id = randomBytes(4).toString('hex');
    const { content } = req.body;
    const postId = req.params.id;
    const comments = commentsByPostId[postId] || [];
    const status = "pending";
    comments.push({
        id,
        content,
        status
    })
    commentsByPostId[postId] = comments;
    await axios.post('http://event-bus-srv:4003/events', {
        type: "CommentCreated",
        data: {
            id,
            content,
            postId,
            status
        }
    });
    res.status(201).send(commentsByPostId[postId]);
});


app.post('/events', async (req, res) => {
    console.log('event received:', req.body);
    const { type, data } = req.body;

    if (type === 'CommentModerated')
    {
        const { postId, id, content, status } = data;
        
        const comment = commentsByPostId[postId];

        comment.status = status;
        comment.content = content;

        await axios.post('http://event-bus-srv:4003/events', {
            type: 'CommentUpdated',
            data: {
                id,
                postId,
                status,
                content
            }
        });
    }

    res.status(200).send({ status: 'OK' });
});


app.listen(4001, () => {
    console.log('Listening in port 4001. (comments)');
});