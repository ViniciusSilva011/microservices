const express = require('express');
const app = express();
const axios = require('axios');
app.use(express.json());


const events = [];

app.post('/events', async (req, res) => {
    const event = req.body;
    events.push(event);
    await axios.post('http://posts-clusterip-srv:4000/events', event); // posts
    await axios.post('http://comments-srv:4001/events', event); // comments
    await axios.post('http://query-srv:4002/events', event); // query service
    await axios.post('http://moderation-srv:4004/events', event); // moderation service
    
    res.send({ status: 'OK' });
});

app.get('/events', (req, res) => {
    res.send(events);
})

app.listen(4003, () => {
    console.log(`listening at port 4003 (event-bus)`)
});