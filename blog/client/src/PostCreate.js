import React, { useState } from 'react';
import axios from 'axios';

export default () => {

    const [title, setTitle] = useState('');

    const doRequest = async (e) => {
        e.preventDefault();
         await axios.post('http://posts.com/posts/create', { title });
        setTitle('');
    }

    return (
        <div>
            <h1>Create post!</h1>

            <form onSubmit={e => doRequest(e)} className="form">
                <label name="title" htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" value={title} onChange={e => setTitle(e.target.value)} />
                <button className="btn btn-primary mt-2">Submit</button>
            </form>

        </div>
    );

}
