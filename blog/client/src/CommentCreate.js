import React, { useState } from 'react';
import axios from 'axios';

export default ({ postId }) => {
    const [content, setContent] = useState('')
    const submitForm = async (e) => {
        e.preventDefault();
         await axios.post(`http://posts.com/posts/${postId}/comments`, {
            content
        })
        setContent('');
    }

    return (
        <form onSubmit={submitForm}>
            <input className="form-control" type="text" onChange={e => setContent(e.target.value)} value={content} />
            <button className="btn btn-sm btn-primary mt-3">Comment</button>
        </form>
    );
}