import React, {useState, useEffect} from 'react';

export default ({ comments }) => {
    



    return (
        <ul> {comments.map(e => {
            let content = "";

            if (e.status === 'approved')
                content = e.content;
            if (e.status === 'rejected')
                content = "This comment was rejected";
            if (e.status === 'pending')
                content = "This comment is awaiting for moderation";

           return <li key={e.id}>{content}</li>
        })}</ul>
    );
}