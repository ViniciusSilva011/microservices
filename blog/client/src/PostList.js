import Axios from 'axios';
import React, { useState, useEffect } from 'react'
import CommentCreate from './CommentCreate';
import CommentList from './CommentList';

export default () => {
    const [posts, setPosts] = useState({});

    const fetchPosts = async () => {
        // const { data } = await Axios.get('http://localhost:4000/posts');
        const { data } = await Axios.get('http://posts.com/posts');
        // console.log(`fromQueryService :`, fromQueryService)
        setPosts(data);
    };

    useEffect(() => {
        fetchPosts();
    }, []);
    
    const renderPosts = Object.values(posts).map(e => {
        return (
            <div className="card" key={e.id}>
                <div className="card-body">
                    <h6>{e.title}</h6>
                    <hr />
                    <CommentList comments={e.comments} />                    
                    <hr />
                    <CommentCreate postId={e.id} />
                </div>
            </div>
        )
    });

    return <div className="d-flex flex-row flex-wrap justify-content-between align-center">
        {renderPosts}
    </div>
}