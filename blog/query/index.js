const express = require('express');
const cors = require('cors');
const axios = require('axios')
const app = express();

app.use(cors());
app.use(express.json());

const posts = {};



app.get('/posts', (req, res) => {
    res.send(posts);
});

const handleEvent = (type, data) => {
    if (type === 'PostCreated') {
        const { id, title } = data;
        posts[id] = { id, title, comments: [] };
    }

    if (type === 'CommentCreated') {
        const { id, content, postId, status } = data;
        
        posts[postId].comments.push(
            { id, content, status }
        )
    }

    if (type === 'CommentUpdated') {
        const { id, content, postId, status } = data;

        console.log(`posts[postId] :`, posts[postId])
        const comments = posts[postId].comments;

        comment = comments.find(e => e.id === id);
        comment.content = content;
        comment.status = status;
        console.log(`comment updated :`, comment)
    }
}

app.post('/events', (req, res) => {
    const { type, data } = req.body;

    handleEvent(type, data);
    console.log(`posts :`, posts)

    res.send({ status: 'OK' });
});

app.listen(4002, async () => {
    console.log(`Listening at port 4002 (query)`);

    const {data} = await axios.get('http://event-bus-srv:4003/events');

    
    for (const re of data) {
        console.log(`Processing: ${re.type}`)
        handleEvent(re.type, re.data);
    }

});
